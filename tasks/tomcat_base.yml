---
- name: install required packages
  yum:
    name:
      - unzip
      - logrotate
    state: present

- name: create tomcat group
  group:
    name: tomcat
    gid: "{{ epicsarchiverap_tomcat_gid }}"
    state: present

- name: create tomcat user
  user:
    name: tomcat
    uid: "{{ epicsarchiverap_tomcat_uid }}"
    group: tomcat
    shell: /sbin/nologin
    create_home: false
    comment: "Apache Tomcat"
    system: true
    state: present

- name: create JMX exporter directory
  file:
    path: "{{ epicsarchiverap_jmx_exporter_jar | dirname }}"
    state: directory
    owner: tomcat
    group: tomcat
    mode: 0770

- name: install JMX exporter
  get_url:
    url: "{{ epicsarchiverap_jmx_exporter_url }}"
    dest: "{{ epicsarchiverap_jmx_exporter_jar }}"
    owner: tomcat
    group: tomcat
    mode: 0644

- name: create tomcat log directory
  file:
    path: /var/log/tomcat
    state: directory
    owner: tomcat
    group: tomcat
    mode: 0770

- name: configure logrotate for arch.log and archappl-policy.log
  copy:
    src: archiver-appliance.logrotate
    dest: /etc/logrotate.d/archiver-appliance
    owner: root
    group: root
    mode: 0644

- name: install tomcat
  unarchive:
    src: "{{ epicsarchiverap_tomcat_archive }}"
    dest: /opt
    remote_src: true
    owner: tomcat
    group: tomcat
    creates: "/opt/apache-tomcat-{{ epicsarchiverap_tomcat_version }}/lib/catalina.jar"

- name: create link to tomcat home
  file:
    src: "/opt/apache-tomcat-{{ epicsarchiverap_tomcat_version }}"
    dest: "{{ epicsarchiverap_tomcat_home }}"
    state: link

- name: install mysql connector
  get_url:
    url: "{{ epicsarchiverap_mysql_connector_url }}"
    dest: "{{ epicsarchiverap_tomcat_home }}/lib"
    owner: tomcat
    group: tomcat
    mode: 0644

# We use our specific tomcat@.service to make it part of
# the archiver-appliance.service (to start all tomcat instances
# with one command)
- name: install archiver-appliance systemd service
  copy:
    src: archiver-appliance.service
    dest: /etc/systemd/system/archiver-appliance.service
    owner: root
    group: root
    mode: 0644

- name: install tomcat@.service
  template:
    src: tomcat@.service.j2
    dest: /etc/systemd/system/tomcat@.service
    owner: root
    group: root
    mode: 0644

# Don't start the service as the tomcat instances are not installed yet
- name: enable the archiver-appliance service
  systemd:
    name: archiver-appliance
    enabled: true
    daemon_reload: true

- name: create the common log4j.properties
  template:
    src: log4j.properties.j2
    dest: "{{ epicsarchiverap_tomcat_home }}/lib/log4j.properties"
    owner: root
    group: root
    mode: 0644
  notify:
    - restart archiver-appliance

- name: create tomcats base directory
  file:
    path: "{{ epicsarchiverap_tomcats_base }}"
    state: directory
    owner: root
    group: root
    mode: 0755

- name: create the appliances.xml file
  template:
    src: appliances.xml.j2
    dest: "{{ epicsarchiverap_tomcats_base }}/appliances.xml"
    owner: root
    group: tomcat
    mode: 0644
  notify:
    - restart archiver-appliance

- name: create storage directories
  file:
    path: "{{ epicsarchiverap_storage }}/{{ item }}/ArchiverStore"
    state: directory
    owner: tomcat
    group: tomcat
    mode: 0755
  loop:
    - sts
    - mts
    - lts

- name: create archappl release directory
  file:
    path: "{{ epicsarchiverap_release_dir }}"
    state: directory
    owner: root
    group: root
    mode: 0755

- name: unarchive archappl release
  unarchive:
    src: "{{ epicsarchiverap_url }}"
    dest: "{{ epicsarchiverap_release_dir }}"
    remote_src: true
    creates: "{{ epicsarchiverap_release_dir }}/mgmt.war"

- name: create link to archappl release directory
  file:
    src: "{{ epicsarchiverap_release_dir }}"
    dest: "{{ epicsarchiverap_release_root }}/archappl"
    state: link
  register: archappl_release

# Skip Ansible lint to avoid:
# [ANSIBLE0016] Tasks that run when changed should likely be handlers
# This is not a handler - we want it to run now and not at the end of the play
- name: delete all tomcat instances webapps to force to re-deploy
  file:
    path: "{{ epicsarchiverap_tomcats_base }}/{{ item.name }}/webapps"
    state: absent
  loop: "{{ epicsarchiverap_tomcat_instances }}"
  when: archappl_release.changed
  tags:
    - skip_ansible_lint
